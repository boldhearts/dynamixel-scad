include <colors.scad>
include <mx-common.scad>

function hn07_i101_height() = 3;

module hn07_i101() {
  color(Metal) {
    difference() {
      union() {
        cylinder(r=6.25, h=3.);
        translate([0, 0, 1]) cylinder(r=11, h=2);
      }

      cylinder(h=7, r=5, center=true);
      translate([0, 0, 2.3]) cylinder(h=3, r=5.8);

      hole_ring(8, 8, 1, 4);
    }
  }
}

hn07_i101();
