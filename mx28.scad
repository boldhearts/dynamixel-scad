use <mx-common.scad>
use <hn07-n101.scad>
use <hn07-i101.scad>

include <colors.scad>

box_size = [45, 24.4, 35.5];
rim_size = [50.6, 35.6];

height = 5.5 / 2;
screw_offset = 3;
screw_radius = 2;

axis_offset=10.5;

function mx28_horn_to_bearing() = box_size[2] + hn07_n101_height() + hn07_i101_height();

module mx28(with_horn) {
  color(BlackPlastic) {
    difference() {
      translate([axis_offset, 0, 0]) union() {
        difference() {
          mx_box(box_size);
          translate([0, 0, -box_size[2] / 2]) bottom_mold(box_size, height=2.75, rounding_radius=1.5);
        }
        rim(box_size, z=10.5, hole_x_offset=7.5, hole_distance=17);
        mirror([0, 0, 1]) rim(box_size, z=10.5, hole_x_offset=7.5, hole_distance=17);
      }
      translate([0, 0, -box_size[2] / 2]) cylinder(h=1, r=6.5);
    }
    translate([0, 0, -box_size[2] / 2 + 1]) {
      difference() {
        union() {
          translate([0, 0, -0.3]) cylinder(h=0.3, r=4);
          translate([0, 0, -2.5]) cylinder(h=2.5, r=3);
        }
        translate([0, 0, -2.5]) cylinder(h=4.4, r=1.6, center=true);
      }
    }
  }

  translate([0, 0, box_size[2] / 2]) {
    color(Metal) horn_connector(4.85, 2.85);
  }

  if (with_horn) {
    translate([0, 0, box_size[2] / 2]) hn07_n101();
    translate([0, 0, -box_size[2] / 2]) mirror([0, 0, 1]) hn07_i101();
  }

}

mx28(with_horn=true);
