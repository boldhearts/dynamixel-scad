include <colors.scad>
include <mx-common.scad>

module hn05_n102() {
  color(Metal) {
    difference() {
      union() {
        cylinder(h=5.1, r=5);
        translate([0, 0, 0.8]) cylinder(h=2.5, r=14);
      }
      translate([0, 0, -1]) {
        horn_connector(height=3.9, radius=4);
        cylinder(h=7, r=1.65);
      }
      hole_ring(4, 8, 1, 4);
      hole_ring(8, 11, 1.25, 4);
    }
  }

  translate([0, 0, 0.25]) color(BlackPlastic) washer(9, 5, 0.75);
}

hn05_n102();
