use <mx-common.scad>
use <hn05-n102.scad>
use <hn05-i101.scad>

include <colors.scad>

box_size = [59.5, 29, 46];
axis_offset = 15.25;

/** Custom rim with threaded holes instead of nut holders
 */
module threaded_rim(box_size, z, hole_x_offset, hole_distance,
                    width=5.6, height=3.5, bolt_holder_height=1.5, hole_radius=1.05) {
  size = [box_size[0] + width, box_size[1] + 2 * width];
  hole_offset = width / 2;
  bolt_holder_width = 4;

  holes_xy = rim_holes_xy([size[0], size[1]], hole_x_offset, hole_distance, hole_offset);

  translate([hole_offset, 0, z]) {
    intersection() {
      difference() {
        union() {
          // Main plane
          translate([0, 0, bolt_holder_height]) rim_main(size, height - bolt_holder_height);

          // Thickeners for holes
          for (xy = concat(holes_xy[0], holes_xy[1]))
            translate(xy)
              linear_extrude(bolt_holder_height) square([bolt_holder_width, width], center=true);
          for (xy = concat(holes_xy[2]))
            translate(xy)
              linear_extrude(bolt_holder_height) square([width, bolt_holder_width], center=true);
        }

        // Make screw holes
        for (xy = concat(holes_xy[0], holes_xy[1], holes_xy[2]))
          translate(concat(xy, [height / 2])) cylinder(h=1.2 * height, r=hole_radius, center=true);
      }

      // Cut off corners
      linear_extrude(height) beveled_square(size, [hole_x_offset -1, width], center=true);
    }
  }
}

module mx106(with_horn) {
  color(BlackPlastic) {
    difference() {
      translate([axis_offset, 0, 0]) union() {
        difference() {
          mx_box(box_size);
          translate([0, 0, -box_size[2] / 2]) bottom_mold(box_size, height=6, rounding_radius=1.5);
        }
        threaded_rim(box_size, z=13.5, hole_x_offset=9, hole_distance=22);
        mirror([0, 0, 1]) rim(box_size, z=12.5, hole_x_offset=9, hole_distance=22);
      }
      translate([0, 0, -box_size[2] / 2]) cylinder(h=1, r=7.5);
    }

    translate([0, 0, -box_size[2] / 2 + 1]) {
      difference() {
        union() {
          translate([0, 0, -0.3]) cylinder(h=0.3, r=5);
          translate([0, 0, -2.5]) cylinder(h=2.5, r=4);
        }
        translate([0, 0, -2.5]) cylinder(h=4.4, r=1.6, center=true);
      }
    }
  }

  translate([0, 0, box_size[2] / 2]) {
     color(Metal) horn_connector(height=3.9, radius=4); 
  }

  if (with_horn) {
    translate([0, 0, box_size[2] / 2]) hn05_n102();
    translate([0, 0, -box_size[2] / 2]) mirror([0, 0, 1]) hn05_i101();
  }

}

mx106(with_horn=true);
