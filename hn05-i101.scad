include <colors.scad>
include <mx-common.scad>

function hno5_i101_height() = 3.8;

module hn05_i101() {
  color(Metal) {
    difference() {
      union() {
        cylinder(r=7.25, h=3.8);
        translate([0, 0, 1.3]) cylinder(r=14, h=2.5);
      }

      cylinder(h=8, r=6, center=true);
      translate([0, 0, 2.8]) cylinder(h=2, r=7);

      hole_ring(8, 11, 1.25, 4);
    }
  }
}

hn05_i101();
