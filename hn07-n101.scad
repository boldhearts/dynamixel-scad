include <colors.scad>
include <mx-common.scad>

function hn07_n101_height() = 3;

module hn07_n101() {
  color(Metal) {
    difference() {
      union() {
        cylinder(h=5.85, r=4);
        translate([0, 0, 1]) cylinder(h=2, r=11);
      }
      translate([0, 0, -1]) {
        horn_connector(1 + 4.85, 2.85);
        cylinder(h=7, r=1.3);
      }
      hole_ring(8, 8, 1, 4);
    }
  }

  translate([0, 0, 0.25]) color(WhitePlastic) washer(11, 4, 0.75);
}

hn07_n101();
