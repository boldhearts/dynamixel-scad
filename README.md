# DYNAMIXEL-Scad

OpenSCAD scripts for popular DYNAMIXEL motors and parts. Currently the
following motors are modelled:

* MX-28, including horn HN07-N101 and bearing HN07-I101
* MX-64, including horn HN05-N102 and bearing HN05-I101
* MX-106, including horn HN05-N102 and bearing HN05-I101

![all motors in OpenScad](allmotors.png)
